import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
from scipy.optimize import curve_fit
from pandas.plotting import register_matplotlib_converters
register_matplotlib_converters()


START_DATE = '1/22/20'

# From: https://de.wikipedia.org/wiki/COVID-19-Pandemie_in_Deutschland
EVENTS = {'Germany': {pd.to_datetime(k): v
                      for k, v in {#'3/19/20': 'DLR',
                                   '3/8/20': {'title': 'No more big events', 'color': 'lightpink'},
                                   '3/17/20': {'title': 'Borders/shops closed', 'color': 'orange'},
                                   '3/22/20': {'title': 'Lockdown', 'color': 'crimson'},
                                   '3/26/20': {'title': 'Test criteria broadened', 'color': 'darkcyan', 'type': 'single'},
                                   '4/20/20': {'title': 'Small shops opening', 'color': 'palevioletred'}
                                   }.items()
                     }
         }

def parse_wikipedia_test_numbers_germany(text):
    bort = {}
    for l in bla.splitlines():
        blub = l.split('\t')
        kw = blub[0]
        if not kw.startswith('<'):
            kw = int(kw)
            n = int(blub[2].replace('.', ''))
            bort[kw] = n
    return bort

# From: https://de.wikipedia.org/wiki/COVID-19-Pandemie_in_Deutschland#Testkapazit%C3%A4ten,_durchgef%C3%BChrte_Tests_und_Anteil_positiver_Ergebnisse
# https://www.rki.de/DE/Content/InfAZ/N/Neuartiges_Coronavirus/Situationsberichte/Gesamt.html

bla = """< 11 	–8. März 2020 	124.716 	3.892 (3,1 %) 	90 	– 	–
11 	9.–15. März 	127.457 	7.582 (5,9 %) 	114 	– 	28
12 	16.–22. März 	348.619 	23.820 (6,8 %) 	152 	– 	93
13 	23.–29. März 	361.515 	31.414 (8,7 %) 	151 	– 	111
14 	30. März–5. April 	408.348 	36.885 (9,0 %) 	154 	– 	113
15 	6.–12. April 	380.197 	30.791 (8,1 %) 	164 	– 	132
16 	13.–19. April 	331.902 	22.082 (6,7 %) 	168 	730.156 	112
17 	20.–26. April 	363.890 	18.083 (5,0 %) 	178 	818.426 	126
18 	27. April–3. Mai 	326.788 	12.608 (3,9 %) 	175 	860.494 	133
19 	4.–10. Mai 	403.875 	10.755 (2,7 %) 	182 	964.962 	137
20 	11.–17. Mai 	432.666 	7.233 (1,7 %) 	183 	1.038.223 	134
21 	18.–24. Mai 	353.467 	5.218 (1,5 %) 	179 	1.050.676 	136
22 	25.–31. Mai 	405.269 	4.310 (1,1 %) 	178 	1.017.179 	143
23 	1.–7. Juni 	340.986 	3.208 (0,9 %) 	176 	1.083.345 	137
24 	8.–14. Juni 	326.645 	2.816 (0,9 %) 	172 	1.092.448 	139
25 	15.–21. Juni 	387.484 	5.309 (1,4 %) 	175 	1.099.355 	138
26 	22.–28. Juni 	466.459 	3.670 (0,8 %) 	179 	1.112.075 	137
27 	29. Juni–5. Juli 	504.082 	3.080 (0,6 %) 	149 	1.118.354 	137
28 	6.–12. Juli 	510.103 	2.990 (0,6 %) 	178 	1.174.960 	145
29 	13.–19. Juli 	538.229 	3.483 (0,6 %) 	176 	1.178.008 	146
30 	20.–26. Juli 	572.311 	4.464 (0,8 %) 	181 	1.182.599 	145
31 	27. Juli–2. August 	586.620 	5.738 (1,0 %) 	170 	1.203.852 	145
32 	3.–9. August 	736.171 	7.335 (1,0 %) 	169 	1.167.188 	149
33 	10.–16. August 	864.004 	8.398 (1,0 %) 	191 	1.220.992 	151
34 	17.–23. August 	1.094.506 	9.233 (0,8 %) 	199	1.267.655 	157
35 	24.–30. August 	1.121.214 	8.324 (0,7 %) 	192	1.402.475 	163
36 	31. August–6. September 	1.099.560 	8.175 (0,7 %) 	192 	1.345.787 	168
37 	7.–13. September 	1.162.133 	10.025 (0,9 %) 	193 	1.440.471 	168
38 	14.–20. September 	1.149.257 	13.279 (1,2 %) 	203 	1.455.142 	165
39 	21.–27. September 	1.167.870 	14.295 (1,2 %) 	190 	1.516.162 	170
40 	28. September–4. Oktober 	1.103.455 	18.356 (1,7 %) 	192 	1.541.289 	168
41 	5.–11. Oktober 	1.186.206 	29.540 (2,5 %) 	187 	1.573.748 	166
42 	12.–18. Oktober 	1.195.661 	43.308 (3,6 %) 	183 	1.712.246 	164 """

NR_TESTS_PER_CALWEEK = {'Germany': {k: v/7.0 for k, v in parse_wikipedia_test_numbers_germany(bla).items()}}

COLORS = plt.rcParams['axes.prop_cycle'].by_key()['color']

def exp_T(x, A, T, Off=0):
    return A * np.exp( np.log(2) * x / T ) + Off


def gauss(x, A, sigma, center):
    return A * np.exp(- (x - center)**2 / (2 * sigma**2 ))


C_PRIO = ['Germany', 'US', 'Italy', 'China']
def country_prio(c):
    if c in C_PRIO:
        return 0, C_PRIO.index(c)
    else:
        return 1, c

    
class CovidPlot(object):
    
    def __init__(self):
        self.dataframe = pd.read_csv('https://github.com/CSSEGISandData/COVID-19/raw/master/csse_covid_19_data/csse_covid_19_time_series/time_series_covid19_confirmed_global.csv')
        self.dataframe_rec = pd.read_csv('https://github.com/CSSEGISandData/COVID-19/raw/master/csse_covid_19_data/csse_covid_19_time_series/time_series_covid19_recovered_global.csv')
        self.dataframe_deaths = pd.read_csv('https://github.com/CSSEGISandData/COVID-19/raw/master/csse_covid_19_data/csse_covid_19_time_series/time_series_covid19_deaths_global.csv')
        self.countries = sorted(list(set(self.dataframe['Country/Region'])),
                                key=country_prio)
        fig = plt.figure(num='COVID-19 Cases', figsize=(10, 10))
        self.ax_tot = fig.add_axes([0.1, 0.6, 0.82, 0.36], label='Horst')
        self.ax_tot.set_title('Wurst')
        self.ax_new = fig.add_axes([0.1, 0.32, 0.82, 0.26])
        self.ax_new.set_title('Wurst')
        self.ax_rel = fig.add_axes([0.1, 0.04, 0.82, 0.26])
        self.ax_rel.set_title('Wurst')
        self.ax_tests = self.ax_new.twinx()
        rows = self.dataframe.loc[:,START_DATE:]
        cols = rows.keys()
        self.dates = pd.to_datetime(cols)
        self.weekends = pd.DataFrame([1 if d.weekday() > 4 else 0 for d in self.dates], self.dates)
        self.tests_per_day = {'Germany': pd.DataFrame([NR_TESTS_PER_CALWEEK.get('Germany', {}).get(d.weekofyear) for d in self.dates], self.dates)}
        self.days = np.arange(len(self.dates))
        self.fig = fig
        fig.show()

    
    def plot_country(self, country,# future_weeks=26,
                     log_tot=False,
                     log_new=False,
                     log_rel=False
                     #gaussfit=False,
                     #plot_initial=False
                    ):
        gaussfit = False
        plot_initial = False
        self.ax_tot.clear()
        self.ax_new.clear()
        self.ax_tests.clear()
        self.ax_rel.clear()

        dataframe = self.dataframe[self.dataframe['Country/Region'] == country].loc[:,START_DATE:]
        dataframe_rec = self.dataframe_rec[self.dataframe_rec['Country/Region'] == country].loc[:,START_DATE:]
        dataframe_deaths = self.dataframe_deaths[self.dataframe_deaths['Country/Region'] == country].loc[:,START_DATE:]
        df = pd.DataFrame(dataframe.sum(), self.dates)
        df_rec = pd.DataFrame(dataframe_rec.sum(), self.dates)
        df_deaths = pd.DataFrame(dataframe_deaths.sum(), self.dates)
        #A, T, Off = curve_fit(exp_T, self.days, df[0])[0]
        p0 = [10**4, 8, 60]
        bounds = ([1000, 1, 0], [1E7, 1E4, 1E4])
        if gaussfit:
            try:
                A_g, sigma, center = curve_fit(gauss, self.days, df[0], p0, bounds=bounds)[0]
                print(A_g, sigma, center)
            except RuntimeError:
                gaussfit = False
        #dates_proj = pd.date_range(self.dates[0], self.dates[-1] + pd.Timedelta(weeks=future_weeks))
        #days_proj = np.arange(len(dates_proj))
        
        #proj = pd.DataFrame(exp_T(days_proj, A, T, Off))
        self.ax_tot.plot(df, 'o', label='Confirmed cases', markersize=3)
        self.ax_tot.plot(df_rec, 'og', label='Recovered (JHU guesstimate)', zorder=-5, markersize=3)
        self.ax_tot.plot(df_deaths, 'ok', label='Deaths', zorder=-10, markersize=3)
        self.ax_tot.plot(df - df_rec - df_deaths, 'o', c='magenta', label='Currently infected (JHU guesstimate)', zorder=-2, markersize=3)
        self.ax_tot.plot(df.diff().rolling(window=14).sum(), 'o', c='skyblue', label='Currently infected (my guesstimate)', zorder=-2, markersize=3)
        
                         #dates_proj, proj
        self.ax_tot.plot(self.tests_per_day.get(country, []), 's', label='# Tests per day', color='darkcyan', alpha=0.5, markersize=3)
        
        events = EVENTS.get(country)
        if events is not None:
            evs_span = sorted(list(events.items()))
            for ii, (ev_date, ev) in enumerate(evs_span):
                ev_title = ev['title']
                col = ev['color']
                ev_end = evs_span[ii + 1][0] if ii < len(evs_span) - 1 else self.dates[-1]
                ev_end = self.dates[-1]
                if ii < len(evs_span) - 1:
                    d_fol = [d for d, e in evs_span[ii + 1:] if not e.get('type') == 'single']
                    if d_fol:
                        ev_end = d_fol[0]
                self.ax_tot.axvline(ev_date, label=ev_title, color=col, alpha=0.5)
                self.ax_tot.axvline(ev_date + pd.Timedelta(weeks=2), label='+ two weeks', color=col, linestyle='--', alpha=0.3)
                
                if not ev.get('type') == 'single':
                    self.ax_tot.axvspan(ev_date, ev_end, color=col, alpha=0.1, zorder=-20)
                
                self.ax_new.axvline(ev_date, color=col, alpha=0.5)
                self.ax_new.axvline(ev_date + pd.Timedelta(weeks=2), color=col, linestyle='--', alpha=0.3)
                #self.ax_tot.text(ev_date, 0.9 * df.max(), ev_title)
                
        if plot_initial:
            self.ax_tot.plot(dates_proj, gauss(days_proj, *p0), '--', alpha=0.2)
        if gaussfit:
            sim = gauss(days_proj, A_g, sigma, center)
            self.ax_tot.plot(dates_proj, sim, '--')
            self.ax_tot.set_ylim([0, sim.max()])
        #else:
        #    self.ax_tot.set_ylim([0, df[0].max()*1.1])
        new_cases = df.diff()
        self.ax_new.plot(new_cases, 'o', label='New conf. cases', markersize=3)
        self.ax_new.fill_between(self.dates, self.weekends[0] * new_cases[0].max(), step='pre', alpha=0.2, label='Weekends')
        tests = self.tests_per_day.get(country, pd.DataFrame())
                
        if not tests.empty:
            rel = new_cases / tests
            self.ax_rel.fill_between(self.dates, self.weekends[0] * rel[0].max(), step='pre', alpha=0.2, label='Weekends')
            self.ax_tests.plot(tests, 's', label='# Tests per day', color='darkcyan', alpha=0.5, markersize=3)
            self.ax_rel.plot(rel, 's', label='# Relative positive', color='darkred', alpha=0.5, markersize=3)
        #self.ax_new.plot(dates_proj, proj.diff())
        #self.ax_tot.text(0.25, 0.9,
        #                 'Total # of cases\n# cases doubles every {0:.1f} days'.format(T),
        #                 transform=self.ax_tot.transAxes, bbox={'facecolor': 'lightgrey'},
        #                 horizontalalignment='center', verticalalignment='center')
        #self.ax_new.text(0.25, 0.9,
        #                 '# of new cases per day',
        #                 transform=self.ax_new.transAxes, bbox={'facecolor': 'lightgrey'},
        #                 horizontalalignment='center', verticalalignment='center')
        
        self.ax_tot.legend(loc='center left', fontsize='x-small')
        self.ax_new.legend(loc='upper left')
        self.ax_rel.legend(loc='upper left')
        self.ax_tests.legend(loc='center left')
        self.ax_tests.tick_params(axis='y', labelcolor='darkcyan')
        if log_tot:
            self.ax_tot.set_yscale('log')
        if log_rel:
            self.ax_rel.set_yscale('log')
        if log_new:
            self.ax_new.set_yscale('log')
            self.ax_tests.set_yscale('log')
            self.ax_tests.set_ylim([10, self.ax_tests.get_ylim()[1]])
        else:
            self.ax_tests.set_ylim([0, self.ax_tests.get_ylim()[1]])
        
            